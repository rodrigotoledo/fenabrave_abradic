# encoding: utf-8
begin
  require File.join(File.dirname(__FILE__), 'lib', 'ftp')
rescue LoadError
  puts "'lib/ftp' nao encontrado."
  exit
end

begin
  require File.join(File.dirname(__FILE__), 'lib', 'import_file')
rescue LoadError
  puts "'lib/import_file' nao encontrado."
  exit
end

ftp = FTP.new(:monthly)
import = ImportFile.new
import.do_monthly_import